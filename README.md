# wp-forum

Maintenance fork for the obsoleted [wp-forum] wordpress plugin.

Make it compatible with newer PHP versions.

[wp-forum]: https://wordpress.org/plugins/wpforum/
